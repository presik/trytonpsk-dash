-- We need drop column model because has type Integer for
-- recreate a new column with type Varchar

ALTER TABLE dash_report DROP COLUMN model;

DROP TABLE dash_access_dash_report_rel;
