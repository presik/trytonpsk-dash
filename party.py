from trytond.modules.dash.dash import DashAppBase


class AppParty(DashAppBase):
    "App Party"
    __name__ = "dash.app.party"
